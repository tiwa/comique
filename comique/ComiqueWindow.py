# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# This file is in the public domain
### END LICENSE

import gettext
from gettext import gettext as _
gettext.textdomain('comique')

from gi.repository import Gtk # pylint: disable=E0611
import logging
logger = logging.getLogger('comique')

from comique_lib import Window
from comique.AboutComiqueDialog import AboutComiqueDialog
from comique.PreferencesComiqueDialog import PreferencesComiqueDialog

# See comique_lib.Window.py for more details about how this class works
class ComiqueWindow(Window):
    __gtype_name__ = "ComiqueWindow"
    
    def finish_initializing(self, builder): # pylint: disable=E1002
        """Set up the main window"""
        super(ComiqueWindow, self).finish_initializing(builder)

        self.AboutDialog = AboutComiqueDialog
        self.PreferencesDialog = PreferencesComiqueDialog

        # Code for other initialization actions should be added here.

